# Lafi-bala
This application manages volunteer workers for the annual outdoor festival run by the Chambéry Solidarité International Association. Volunteers enter their preferred dates, times, and jobs on the entry form, and the Association assigns work based on the volunteer's preferred dates and jobs.

## Requirement
- PHP >= 7.4
- composer
- yarn

## Install
1. Download the repository
2. Edit `DATABASE_URL` and `MAILER_DSN` in the .env file
3. Run `composer install`
4. Run `yarn install` and `yarn build`
5. Create database
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
```
