/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import "./styles/app.scss";

// start the Stimulus application
import "./bootstrap";

// addBtn.addEventListener("click", function (event) {
//   event.preventDefault();
//   let elements = document.getElementById("target");
//   let copied = elements.lastElementChild.cloneNode(true);
//   copied.name=
//   elements.appendChild(copied);
// });

$(document).ready(function () {
  $(".add-collection-widget").click(function (e) {
    e.preventDefault();
    let list = $($(this).attr("data-list-selector"));

    let counter = list.data("widget-counter") || list.children().length;
    let arrayEmbeded = document.getElementById("embeded-fields").children;

    let arrayEmbededLength = 1;
    arrayEmbeded.forEach((item, index) => {
      if (item.innerHTML !== "") {
        arrayEmbededLength++;
      }
    });

    if (arrayEmbededLength <= 5) {
      let newWidget = list.attr("data-prototype");

      newWidget = newWidget.replace(/__name__/g, counter);
      newWidget = strIns(newWidget, 4, ' class="row"');
      newWidget = newWidget.replaceAll("mb-3", "mb-3 col-md-3");

      counter++;

      list.data("widget-counter", counter);

      let newElem = $(list.attr("data-widget-tags")).html(newWidget);
      newElem.appendTo(list);
    }
  });
});

function strIns(str, idx, val) {
  let res = str.slice(0, idx) + val + str.slice(idx);
  return res;
}

$("#embeded-fields").on("click", ".delButton", function (e) {
  let id = this.id;
  let removeId = "#" + id.slice(0, -7);
  $(removeId).remove();
});
