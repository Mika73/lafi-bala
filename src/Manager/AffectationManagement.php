<?php

namespace App\Manager;

use App\Entity\Affectation;

// decouper l'algo en fonction le rendre propre

class AffectationManagement
{
	const MAXPRIORITY = 3;

	public function process($slotRepository, $availableRepository, $priorityRepository)
	{
		$temporary_affectations = [];
		$completSlots = [];
		$slots = $slotRepository->findSlotsByAvailables();
		$availables = $availableRepository->findAllUserIdNotNull();
		foreach ($slots as $slot) {
			$task = $slot[0]->getTask();
			$requiredUser = $slot[0]->getRequiredUsers();
			$userCounter = 0;

			for ($i = 0; $i <= self::MAXPRIORITY; $i++) {

				foreach ($availables as $available) {
					if ($this->slotIsInAvailable($slot[0], $available)) {
						$user = $available->getUser();
						if ($this->userIsAvailable($slot[0], $temporary_affectations, $user)) {
							$priority = $user->getTaskPriority($task);
							if ($priority && $priority->getPriority() === $i) {

								if ($requiredUser > $userCounter) {
									$temporary_affectations = $this->affect($available, $slot[0], $temporary_affectations);
									$userCounter++;

									if ($requiredUser === $userCounter) {
										array_push($completSlots, $slot[0]->getId());
										break;
									}
								};
							}
						}
					}
				}
			}
		}
		return [$temporary_affectations, $completSlots];
	}
	private function slotIsInAvailable($slot, $available) // vocation à être dans l'entité available
	{
		if ($slot->getBeginDateTime() < $available->getBeginDateTime()) {
			return false;
		}
		if ($slot->getEndDateTime() > $available->getEndDateTime()) {
			return false;
		}
		return true;
	}

	private function affect($available, $slot, $temporary_affectations)
	{
		$affectation = new Affectation();
		$affectation->setUser($available->getUser());
		$affectation->setSlot($slot);
		array_push($temporary_affectations, $affectation);
		return $temporary_affectations;
	}

	private function userIsAvailable($slot, $temporary_affectations, $user)
	{
		foreach ($temporary_affectations as $affectation) { // intérieur boucle dans entité Affectation
			if ($affectation->getUser() === $user) {
				if ($slot->getBeginDateTime() <= $affectation->getSlot()->getBeginDateTime() && $slot->getEndDateTime() >= $affectation->getSlot()->getBeginDateTime()) {
					return false;
				}
				if ($slot->getBeginDateTime() <= $affectation->getSlot()->getBeginDateTime() && $slot->getEndDateTime() <= $affectation->getSlot()->getEndDateTime()) {
					return false;
				}
				if ($slot->getBeginDateTime() < $affectation->getSlot()->getEndDateTime() && $slot->getEndDateTime() >= $affectation->getSlot()->getEndDateTime()) {
					return false;
				}
			}
		}
		return true;
	}
}
