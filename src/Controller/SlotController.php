<?php

namespace App\Controller;

use DateTime;
use App\Entity\Slot;
use App\Form\SlotType;
use App\Form\SlotAutoType;
use App\Repository\SlotRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/slot")
 */
class SlotController extends AbstractController
{
	/**
	 * @Route("/", name="slot_index", methods={"GET"})
	 */
	public function indexOrderByDateTime(SlotRepository $slotRepository, PaginatorInterface $paginator, Request $request): Response
	{
		$slots = $paginator->paginate(
			$slotRepository->findAllOrderByDateTime(),
			$request->query->getInt('page', 1), /*page number*/
			20 /*limit per page*/
		);
		return $this->render('slot/index.html.twig', [
			'slots' => $slots,
			'authenticated_user'  => $this->getUser()
		]);
	}
	/**
	 * @Route("/task", name="slot_index_task", methods={"GET"})
	 */
	public function indexOrderBytaskName(SlotRepository $slotRepository, PaginatorInterface $paginator, Request $request): Response
	{
		$slots = $paginator->paginate(
			$slotRepository->findAllOrderByTaskName(),
			$request->query->getInt('page', 1), /*page number*/
			20 /*limit per page*/
		);
		return $this->render('slot/index.html.twig', [
			'slots' => $slots,
			'authenticated_user'  => $this->getUser()
		]);
	}
	/**
	 * @Route("/new", name="slot_new", methods={"GET","POST"})
	 */
	public function new(Request $request): Response
	{
		$slot = new Slot();
		$form = $this->createForm(SlotType::class, $slot);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($slot);
			$entityManager->flush();

			return $this->redirectToRoute('slot_index_task', [], Response::HTTP_SEE_OTHER);
		}

		return $this->renderForm('slot/new.html.twig', [
			'slot' => $slot,
			'form' => $form,
			'authenticated_user'  => $this->getUser()
		]);
	}

	/**
	 * @Route("/{id}", name="slot_show", methods={"GET"})
	 */
	public function show(Slot $slot): Response
	{
		return $this->render('slot/show.html.twig', [
			'slot' => $slot,
			'authenticated_user'  => $this->getUser()
		]);
	}

	/**
	 * @Route("/{id}/edit", name="slot_edit", methods={"GET","POST"})
	 */
	public function edit(Request $request, Slot $slot): Response
	{
		$form = $this->createForm(SlotType::class, $slot);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute('slot_index_task', [], Response::HTTP_SEE_OTHER);
		}

		return $this->renderForm('slot/edit.html.twig', [
			'slot' => $slot,
			'form' => $form,
			'authenticated_user'  => $this->getUser()
		]);
	}

	/**
	 * @Route("/{id}", name="slot_delete", methods={"POST"})
	 */
	public function delete(Request $request, Slot $slot): Response
	{
		if ($this->isCsrfTokenValid('delete' . $slot->getId(), $request->request->get('_token'))) {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove($slot);
			$entityManager->flush();
		}

		return $this->redirectToRoute('slot_index', [], Response::HTTP_SEE_OTHER);
	}
	/**
	 *   * Automatically generate slots by specifying the time
	 * @Route("/new/auto", name="slot_new_auto", methods={"GET","POST"})
	 */
	public function newAutoGeneration(Request $request, EntityManagerInterface $manager): Response
	{
		$form = $this->createForm(SlotAutoType::class);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {;
			$date = new DateTime($form->get('date')->getData()->format('Y-m-d'));
			$begintime = $form->get('beginTime')->getData();
			$endtime = $form->get('endTime')->getData();

			$beginDateTime = new DateTime($date->format('Y-m-d') . ' ' . $begintime->format('H:i:s'));

			$endDateTime = new DateTime($date->format('Y-m-d') . ' ' . $endtime->format('H:i:s'));
			$interval = $form->get('interval')->getData();

			$slotEndTime = clone $beginDateTime;
			while ($slotEndTime < $endDateTime) {
				$slot = new Slot();
				$slot->setBeginDateTime($beginDateTime);
				$slotEndTime->add(new \DateInterval($interval));
				$slot->setEndDateTime($slotEndTime);
				$slot->setTask($form->get('task')->getData());
				$slot->setRequiredUsers($form->get('requiredUsers')->getData());
				$manager->persist($slot);
				$manager->flush();
				$beginDateTime->add(new \DateInterval($interval));
			}

			return $this->redirectToRoute('slot_index', [], Response::HTTP_SEE_OTHER);
		}

		return $this->renderForm('slot/new_auto.html.twig', [
			'form' => $form,
			'authenticated_user'  => $this->getUser()
		]);
	}
}
