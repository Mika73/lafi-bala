<?php

namespace App\Controller;

use App\Entity\Affectation;
use App\Form\AffectationType;
use App\Repository\SlotRepository;
use App\Manager\AffectationManagement;
use App\Repository\PriorityRepository;
use App\Repository\AvailableRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\AffectationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * @Route("/admin/affectation")
 */
class AffectationController extends AbstractController
{
    const DIRECTORY_NAME =  '/files';
    const FILE_NAME =  '/affectation.xlsx';
    /**
     * @Route("/process", name="affectation_process", methods={"GET"})
     */
    public function process(AffectationManagement $affectationManagement, SlotRepository $slotRepository, AvailableRepository $availableRepository, PriorityRepository $priorityRepository, EntityManagerInterface $manager, AffectationRepository $affectationRepository): Response
    {
        $results = $affectationManagement->process($slotRepository, $availableRepository, $priorityRepository);
        foreach ($results[0] as $result) {
            $manager->persist($result);
        }
        $manager->flush();

        $affectations = $affectationRepository->findAllOrderBy();
        // init file system
        $fsObject = new Filesystem();
        $current_dir_path = getcwd();

        //make a new directory
        try {
            $new_dir_path = $current_dir_path . self::DIRECTORY_NAME;
            if (!$fsObject->exists($new_dir_path)) {
                $fsObject->mkdir($new_dir_path);
            }
        } catch (IOExceptionInterface $exception) {
            echo "Error creating directory at" . $exception->getPath();
        }

        // create a new file and add contents

        $new_file_path = $current_dir_path . self::DIRECTORY_NAME . self::FILE_NAME;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $arrayData = [
            ['Post', 'Créneaux date', 'Horaire', 'Nom', 'Prénom', 'Age', 'Email', 'Téléphone', 'Repas']
        ];

        foreach ($affectations as $affectation) {
            $taskName = $affectation->getSlot()->getTask()->getName();
            $slotDate = $affectation->getSlot()->getBeginDateTime()->format('d-m-Y');
            $slotTime = $affectation->getSlot()->getBeginDateTime()->format('H:i') . '-' . $affectation->getSlot()->getEndDateTime()->format('H:i');
            $affectedLastName = $affectation->getUser()->getLastName();
            $affectedFirstName = $affectation->getUser()->getFirstName();
            if ($affectation->getUser()->getBirthDate()) {
                $birthday = $affectation->getUser()->getBirthDate()->format('Ymd');
                $affectedAge = floor((date('Ymd') - $birthday) / 10000);
            } else {
                $affectedAge = '';
            }
            $affectedMail = $affectation->getUser()->getEmail();
            $affectedPhone = $affectation->getUser()->getPhone();
            $affectedMeal = $affectation->getSlot()->getMeal();
            $arrayData[] = [$taskName, $slotDate, $slotTime, $affectedLastName, $affectedFirstName, $affectedAge, $affectedMail, $affectedPhone, $affectedMeal];
        }

        $sheet->fromArray(
            $arrayData,  // The data to set
            NULL,        // Array values with this value will not be set
            'A1'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
        );


        $writer = new Xlsx($spreadsheet);
        $writer->save($new_file_path);

        // try {
        //     $new_file_path = $current_dir_path . self::DIRECTORY_NAME . self::FILE_NAME;

        //     if (!$fsObject->exists($new_file_path)) {
        //         $fsObject->touch($new_file_path);
        //         $fsObject->dumpFile($new_file_path, "Post,Créneaux date,Horaire,Nom,Prénom,Age,Email,Téléphone, Repas\n");
        //         foreach ($affectations as $affectation) {
        //             $taskName = $affectation->getSlot()->getTask()->getName();
        //             $slotDate = $affectation->getSlot()->getBeginDateTime()->format('d-m-Y');
        //             $slotTime = $affectation->getSlot()->getBeginDateTime()->format('H:i') . '-' . $affectation->getSlot()->getEndDateTime()->format('H:i');
        //             $affectedLastName = $affectation->getUser()->getLastName();
        //             $affectedFirstName = $affectation->getUser()->getFirstName();
        //             if($affectation->getUser()->getBirthDate()){
        //                 $birthday = $affectation->getUser()->getBirthDate()->format('Ymd');
        //                 $affectedAge = floor((date('Ymd') - $birthday) / 10000);
        //             }else{
        //                 $affectedAge = '';
        //             }
        //             $affectedMail = $affectation->getUser()->getEmail();
        //             $affectedPhone = $affectation->getUser()->getPhone();
        //             $affectedMeal = $affectation->getSlot()->getMeal();
        //             $fsObject->appendToFile($new_file_path, "$taskName, $slotDate, $slotTime, $affectedLastName, $affectedFirstName, $affectedAge, $affectedMail, $affectedPhone, $affectedMeal\n");
        //         }
        //     }
        // } catch (IOExceptionInterface $exception) {
        //     echo "Error creating file at" . $exception->getPath();
        // }
        return $this->redirectToRoute('affectation_index');
    }
    /**
     * @Route("/", name="affectation_index", methods={"GET"})
     */
    public function index(AffectationRepository $affectationRepository, PaginatorInterface $paginator, Request $request): Response
    {
        // init file system
        $fsObject = new Filesystem();
        $current_dir_path = getcwd();
        $new_dir_path = $current_dir_path . self::DIRECTORY_NAME;

        $affectations = $paginator->paginate(
            $affectationRepository->findAllOrderBy(),
            $request->query->getInt('page', 1), /*page number*/
            20 /*limit per page*/
        );
        return $this->render('affectation/index.html.twig', [
            'affectations' => $affectations,
            'exl_isExist' => $fsObject->exists($new_dir_path)
        ]);
    }
    /**
     * @Route("/delete", name="affectation_delete_all", methods={"GET"})
     */
    public function deleteAll(AffectationRepository $affectationRepository): Response
    {
        // init file system
        $fsObject = new Filesystem();
        $current_dir_path = getcwd();
        $new_dir_path = $current_dir_path . self::DIRECTORY_NAME;

        //remove a directory
        try {
            $fsObject->remove($new_dir_path);
        } catch (IOExceptionInterface $exception) {
            echo "Error deleting directory at" . $exception->getPath();
        }

        $affectationRepository->deleteAll();

        return $this->redirectToRoute('affectation_index', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("/new", name="affectation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $affectation = new Affectation();
        $form = $this->createForm(AffectationType::class, $affectation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($affectation);
            $entityManager->flush();

            return $this->redirectToRoute('affectation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('affectation/new.html.twig', [
            'affectation' => $affectation,
            'form' => $form,
            'authenticated_user'  => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="affectation_show", methods={"GET"})
     */
    public function show(Affectation $affectation): Response
    {
        return $this->render('affectation/show.html.twig', [
            'affectation' => $affectation,
            'authenticated_user'  => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="affectation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Affectation $affectation): Response
    {
        $form = $this->createForm(AffectationType::class, $affectation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('affectation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('affectation/edit.html.twig', [
            'affectation' => $affectation,
            'form' => $form,
            'authenticated_user'  => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="affectation_delete", methods={"POST"})
     */
    public function delete(Request $request, Affectation $affectation): Response
    {
        if ($this->isCsrfTokenValid('delete' . $affectation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($affectation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('affectation_index', [], Response::HTTP_SEE_OTHER);
    }
}
