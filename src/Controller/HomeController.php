<?php

namespace App\Controller;

use App\Repository\SlotRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/home_time", name="home_time")
     */
    public function indexOrderByDateTime(SlotRepository $slotRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isGranted('ROLE_ADMIN')) {
            $slots = $paginator->paginate(
                $slotRepository->findSlotsByAvailablesOrderByDateTime(),
                $request->query->getInt('page', 1), /*page number*/
                20 /*limit per page*/
            );

            return $this->render('home/index.html.twig', [
                'slots' => $slots,
                'authenticated_user' => $this->getUser()
            ]);
        } else {
            return $this->redirectToRoute('set_user_available', [
                'authenticated_user' => $this->getUser()
            ]);
        }
    }
    /**
     * @Route("/", name="home")
     */
    public function indexOrderBytaskName(SlotRepository $slotRepository, PaginatorInterface $paginator, Request $request): Response
    {
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('user_index');
            // $slots = $paginator->paginate(
            //     $slotRepository->findSlotsByAvailablesOrderByTaskName(),
            //     $request->query->getInt('page', 1), /*page number*/
            //     20 /*limit per page*/
            // );

            // return $this->render('home/index.html.twig', [
            //     'slots' => $slots,
            // ]);
        } elseif($this->isGranted('ROLE_USER'))  {
            return $this->redirectToRoute('set_user_available', [
                'authenticated_user' => $this->getUser()
            ]);
        }else{
            return $this->render('home/registration.html.twig');
        }
    }
}
