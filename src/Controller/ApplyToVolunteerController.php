<?php

namespace App\Controller;

use DateTime;
use App\Entity\Priority;
use App\Entity\Available;
use App\Form\PriorityType;
use App\Form\AvailableType;
use App\Repository\SlotRepository;
use App\Repository\TaskRepository;
use App\Repository\PriorityRepository;
use App\Repository\AvailableRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApplyToVolunteerController extends AbstractController
{
	/**
	 * @Route("/apply/volunteer", name="set_user_available")
	 */
	public function setUserAvailable(Request $request, SlotRepository $slotRepository, EntityManagerInterface $manager, AvailableRepository $availableRepository, PriorityRepository $priorityRepository): Response
	{
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
		// Extract the date the event will take place from all slots
		$user = $this->getUser();
		if (count($availableRepository->findByUserId($user->getId())) == 0) {
			$eventDates = $slotRepository->findEventsDates();
			$form = $this->createForm(AvailableType::class, $eventDates);

			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$availabilities[] = ($form->get('availability1')->getData());
				$availabilities[] = ($form->get('availability2')->getData());
				$i = 0;
				foreach ($availabilities as $availability) {

					list($isEnterd, $beginDateTime, $endDateTime, $beginDateTime2, $endDateTime2) = $this->setAvailableTime($eventDates[$i]['eventdate'], $availability);

					// If user has already registered the same period of the time, not persist
					if (!$availableRepository->findOneByTimeAndUser($user, $beginDateTime, $endDateTime) && $isEnterd) {
						$available = new Available();
						$available->setBeginDateTime($beginDateTime);
						$available->setEndDateTime($endDateTime);

						if($beginDateTime2 && $endDateTime2){
							$available->setAvailablePeriod(Available::MORNING);
						}else{
							$available->setAvailablePeriod($availability);
						}
						$available->setUser($user);
						$manager->persist($available);
					}
					// When the user is available in the morning and evening
					if($beginDateTime2 && $endDateTime2){
						if (!$availableRepository->findOneByTimeAndUser($user, $beginDateTime2, $endDateTime2) && $isEnterd) {
							$available = new Available();
							$available->setBeginDateTime($beginDateTime2);
							$available->setEndDateTime($endDateTime2);
							$available->setAvailablePeriod(Available::EVENING);
							$available->setUser($user);
							$manager->persist($available);
						}
					}
					$i++;
				}
				$manager->flush();

				return $this->redirectToRoute('set_priority');
			}
			return $this->render('apply_to_volunteer/index.html.twig', [
				'form' => $form->createView(),
				'authenticated_user'  => $this->getUser()
			]);
		} elseif (count($priorityRepository->findByUserId($user->getId())) == 0) {
			return $this->redirectToRoute('set_priority');
		};
		return $this->redirectToRoute('output_summary');
	}
	static function setAvailableTime($date, $availability)
	{
		if ($availability == '') {
			return [null, null, null, null, null];
		}
		$beginDateTime2 = null;
		$endDateTime2 = null;

		$nextDay = new DateTime($date);
		$nextDay->modify('+1 day');

		if ($availability == Available::MORNING) {
			$beginDateTime = new DateTime($date . ' ' . '07:00:00');
			$endDateTime = new DateTime($date . ' ' . '13:00:00');
		}
		if ($availability == Available::AFTERNOON) {
			$beginDateTime = new DateTime($date . ' ' . '12:00:00');
			$endDateTime = new DateTime($date . ' ' . '18:00:00');
		}
		if ($availability == Available::EVENING) {
			$beginDateTime = new DateTime($date . ' ' . '18:00:00');
			$endDateTime = new DateTime($nextDay->format('d-m-Y') . ' ' . '01:00:00');
		}
		if ($availability == Available::MORNING_AFTERNOON) {
			$beginDateTime = new DateTime($date . ' ' . '07:00:00');
			$endDateTime = new DateTime($date . ' ' . '18:00:00');
		}
		if ($availability == Available::AFTERNOON_EVENING) {
			$beginDateTime = new DateTime($date . ' ' . '12:00:00');
			$endDateTime = new DateTime($nextDay->format('d-m-Y') . ' ' . '01:00:00');
		}
		if ($availability == Available::MORNING_EVENING) {
			$beginDateTime = new DateTime($date . ' ' . '07:00:00');
			$endDateTime = new DateTime($date . ' ' . '13:00:00');
			$beginDateTime2 = new DateTime($date . ' ' . '18:00:00');
			$endDateTime2 = new DateTime($nextDay->format('d-m-Y') . ' ' . '01:00:00');
		}
		if ($availability == Available::ALLDAY) {
			$beginDateTime = new DateTime($date . ' ' . '07:00:00');
			$endDateTime = new DateTime($nextDay->format('d-m-Y') . ' ' . '01:00:00');
		}

		return [true, $beginDateTime, $endDateTime, $beginDateTime2, $endDateTime2];
	}
	/**
	 * @Route("/apply/volunteer/priority", name="set_priority")
	 */
	public function setPriority(Request $request, SlotRepository $slotRepository, AvailableRepository $availableRepository, TaskRepository $taskRepository, PriorityRepository $priorityRepository, EntityManagerInterface $manager): Response
	{
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

		// Compare user available dates and datetimes in the slot table. Extract only task names that the user can participate
		$availables = $availableRepository->findByUserId($this->getUser()->getId());
		$user = $this->getUser();
		$eventsUserCanAttend = [];
		foreach ($availables as $available) {
			$slots = $slotRepository->findAvailableEvents($available->getBeginDateTime(), $available->getEndDateTime());
			foreach ($slots as $slot) {
				array_push($eventsUserCanAttend, $slot->getTask()->getName());
			};
		}
		$eventsUserCanAttend = array_unique($eventsUserCanAttend);

		// If the user doesn't have any possibility, display all task names.
		$arrLength = count($eventsUserCanAttend);
		if ($arrLength === 0) {
			$tasks =
				$taskRepository->findAll();
			foreach ($tasks as $task) {
				array_push($eventsUserCanAttend, $task->getName());
			}
		}

		$form = $this->createForm(PriorityType::class, $eventsUserCanAttend);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$prioritiesRepo = $priorityRepository->findByUserId($this->getUser()->getId());
			for ($i = 1; $i <= 3; $i++) {
				$priorities[] = ($form->get("eventname$i")->getData());
			}
			$j = 1 + count($prioritiesRepo);
			$prirorityArray = [];
			foreach ($prioritiesRepo as $priorityRepo) {
				$task = $taskRepository->findByTaskName($priorityRepo->getTask()->getName());
				array_push($prirorityArray, $task);
			}

			foreach ($priorities as $priority) {
				$task = $taskRepository->findByTaskName($priority);

				// If the user selects a duplicate priority, eliminate it
				if (!in_array($task, $prirorityArray) && $priority != '') {
					$priority = new Priority();
					$priority->setUser($user);
					$priority->setTask($task[0]);
					$priority->setPriority($j);
					$manager->persist($priority);
					array_push($prirorityArray, $task);
					$j++;
				}
			}
			$manager->flush();
			return $this->redirectToRoute('output_summary');
		}
		return $this->render('apply_to_volunteer/register_priorities.html.twig', [
			'form' => $form->createView()
		]);
	}
	/**
	 * @Route("/apply/volunteer/summary", name="output_summary")
	 */
	public function outputSummary(AvailableRepository $availableRepository, PriorityRepository $priorityRepository): Response
	{
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
		$dateStorage = null;
		$formattedAvailables = [];
		$availables = $availableRepository->findByUserId($this->getUser()->getId());
		foreach($availables as $available){
			if($dateStorage == $available->getBeginDateTime()->format('Ymd')){
				//Remove the last array of data from $formattedAvailables
				\array_pop($formattedAvailables);

				$available->setAvailablePeriod(Available::MORNING_EVENING);
				$formattedAvailables[] = $available;
				$dateStorage = $available->getBeginDateTime()->format('Ymd');
			}else{
				$dateStorage = $available->getBeginDateTime()->format('Ymd');
				$formattedAvailables[] = $available;
			}
		}
		$priorities = $priorityRepository->findByUserId($this->getUser()->getId());
		return $this->render('apply_to_volunteer/registration_summary.html.twig', [
			'availables' => $formattedAvailables,
			'priorities' => $priorities,
			'authenticated_user'  => $this->getUser(),
			'userid' => $this->getUser()->getId()
		]);
	}
}
