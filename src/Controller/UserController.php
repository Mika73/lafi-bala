<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Entity\Priority;
use App\Entity\Available;
use App\Form\UserAdminType;
use App\Repository\SlotRepository;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use App\Repository\AvailableRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $users = $paginator->paginate(
            $userRepository->findAll(),
            $request->query->getInt('page', 1), /*page number*/
            20 /*limit per page*/
        );
        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            $user = new User();
            $form = $this->createForm(UserType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
            }
            return $this->renderForm('user/new.html.twig', [
                'user' => $user,
                'form' => $form,
            ]);
        }
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show($id, User $user, AvailableRepository $availableRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        //Only own user's info can be consulted
        //But admin user can consult all user's info
        if ($id == $this->getUser()->getId() || $this->isGranted('ROLE_ADMIN')) {
            if ($this->isGranted('ROLE_ADMIN')) {

                $dateStorage = null;
                $formattedAvailables = [];
                $availables = $availableRepository->findByUserId($id);
                foreach($availables as $available){
                    if($dateStorage == $available->getBeginDateTime()->format('Ymd')){
                        //Remove the last array of data from $formattedAvailables
                        \array_pop($formattedAvailables);

                        $available->setAvailablePeriod(Available::MORNING_EVENING);
                        $formattedAvailables[] = $available;
                        $dateStorage = $available->getBeginDateTime()->format('Ymd');
                    }else{
                        $dateStorage = $available->getBeginDateTime()->format('Ymd');
                        $formattedAvailables[] = $available;
                    }
                }

                return $this->render('user/show_admin.html.twig', [
                    'availables'=> $formattedAvailables,
                    'user' => $user,
                ]);
            }
            return $this->render('user/show.html.twig', [
                'user' => $user,
            ]);
        }
        return $this->redirectToRoute('user_show', [
            'id' => $this->getUser()->getId()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit($id, Request $request, User $user,  EntityManagerInterface $manager,  SlotRepository $slotRepository,  TaskRepository $taskRepository, AvailableRepository $availableRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($id == $this->getUser()->getId() || $this->isGranted('ROLE_ADMIN')) {
            if ($this->isGranted('ROLE_ADMIN')) {
                $form = $this->createForm(UserAdminType::class, $user);
            } else {
                $form = $this->createForm(UserType::class, $user);
            }
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if ($this->isGranted('ROLE_ADMIN')) {

                    // Update Availables
                    $newAvailabiles[] = ($form->get('availability1')->getData());
                    $newAvailabiles[] = ($form->get('availability2')->getData());
                    $i = 0;

                    $eventDates = $slotRepository->findEventsDates();

                    // Delete availables
                    $availables = $user->getAvailables();
                    foreach ($availables as $available) {
                        $user->removeAvailable($available);
                    }

                    foreach ($newAvailabiles as $newAvailable) {

                        list($isEnterd, $beginDateTime, $endDateTime, $beginDateTime2, $endDateTime2) = ApplyToVolunteerController::setAvailableTime($eventDates[$i]['eventdate'], $newAvailable);
                        if ($isEnterd) {
                            $available = new Available();
                            $available->setBeginDateTime($beginDateTime);
                            $available->setEndDateTime($endDateTime);

                            if ($beginDateTime2 && $endDateTime2) {
                                $available->setAvailablePeriod(Available::MORNING);
                            } else {
                                $available->setAvailablePeriod($newAvailable);
                            }
                            $available->setUser($user);
                            $manager->persist($available);
                        }
                        // When the user is available in the morning and evening
                        if ($beginDateTime2 && $endDateTime2) {
                            if (!$availableRepository->findOneByTimeAndUser($user, $beginDateTime2, $endDateTime2) && $isEnterd) {
                                $available = new Available();
                                $available->setBeginDateTime($beginDateTime2);
                                $available->setEndDateTime($endDateTime2);
                                $available->setAvailablePeriod(Available::EVENING);
                                $available->setUser($user);
                                $manager->persist($available);
                            }
                        }
                        $i++;
                    }
                    $manager->flush();

                    // Update priorities
                    for ($i = 1; $i <= 3; $i++) {
                        $newPriorities[] = ($form->get("eventname$i")->getData());
                    }
                    // Delete priorities
                    $priorities = $user->getPriorities();
                    foreach ($priorities as $priority) {
                        $user->removePriority($priority);
                    }
                    $i = 1;
                    foreach ($newPriorities as $newPriority) {
                        $task = $taskRepository->findByTaskName($newPriority);

                        if ($newPriority != '') {
                            $priority = new Priority();
                            $priority->setUser($user);
                            $priority->setTask($task[0]);
                            $priority->setPriority($i);
                            $manager->persist($priority);
                            $i++;
                        }
                    }
                    $manager->flush();
                } else {
                    $this->getDoctrine()->getManager()->flush();
                }

                if ($this->isGranted('ROLE_ADMIN')) {
                    return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
                } else {
                    return $this->redirectToRoute('user_show', [
                        'id' => $this->getUser()->getId()
                    ]);
                }
            }

            if ($this->isGranted('ROLE_ADMIN')) {
                return $this->renderForm('user/edit_admin.html.twig', [
                    'user' => $user,
                    'form' => $form,
                ]);
            }
            return $this->renderForm('user/edit.html.twig', [
                'user' => $user,
                'form' => $form,
            ]);
        }
        return $this->redirectToRoute('user_show', [
            'id' => $this->getUser()->getId()
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
    }
}
