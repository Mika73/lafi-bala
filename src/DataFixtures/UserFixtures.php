<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $passwordHasher;
    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        // $faker = Faker\Factory::create('fr_FR');
        // for ($i = 0; $i < 10; $i++) {
        //     $user = new User();
        //     $user->setFirstName($faker->firstName);
        //     $user->setLastName($faker->lastName);
        //     $user->setEmail($faker->email);
        //     $user->setPhone($faker->phoneNumber);
        //     $user->setGender('gender-' . $i);
        //     $user->setPassword($faker->password);
        //     $user->setPassword($this->passwordHasher->hashPassword(
        //         $user,
        //         'password' . $i
        //     ));
        //     $user->setRoles(['ROLE_USER']);
        //     $manager->persist($user);
        // }
        // $manager->flush();
    }
}
