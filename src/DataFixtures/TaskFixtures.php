<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker;

class TaskFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $taskNames =['Accueil', 'Buvette', 'Loto', 'Spectacle', 'Animation 1', 'Animation 2', 'Animation 3', 'Snack', 'Mise en place du lieu', 'Nettoyage du site'];
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $task = new Task();
            // $task->setName($faker -> word);
            $task->setName($taskNames[$i]);
            $manager->persist($task);
        }
        $manager->flush();
    }
}
