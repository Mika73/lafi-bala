<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserType extends AbstractType
{
    const FEMALE =  'Mme';
    const MALE = 'M';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('firstName', TextType::class, [
				'label' => 'Prénom',
			])
            ->add('lastName', TextType::class, [
				'label' => 'Nom',
			])
            ->add('phone', TextType::class, [
				'label' => 'Téléphone',
			])
            ->add('gender', ChoiceType::class, [
                'choices'  => [
                    self::MALE => self::MALE,
                    self::FEMALE => self::FEMALE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label'=> 'Civilité'
            ])
            ->add('birthDate', BirthdayType::class, [
                'widget' => 'single_text',
                'required'          => false,
                'label'=> 'Date de naissance'
            ])
            // ->add('roles', ChoiceType::class, [
            //     'multiple' => true,
            //     'choices'  => [
            //         'Bénévole' => 'ROLE_USER',
            //         'Organisateur' => 'ROLE_ADMIN',
            //     ]
            // ])
            ->add('comment', TextareaType::class, [
                'attr' => ['class' => 'specific-textarea'],
                'required'          => false,
                'label'=> 'Commentaires aux organisateurs'
            ])
            ->add('newsletter', CheckboxType::class, [
				'required' => false,
                'label'=> 'Je souhaite recevoir les newsletters de Chambéry Solidarité Internationale'
			]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
