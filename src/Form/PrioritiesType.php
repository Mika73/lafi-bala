<?php

namespace App\Form;

use App\Form\PriorityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PrioritiesType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		foreach ($options as $key => $values) {
			if ($key === 'data') {
				$eventNames = $values;
			}
		}
	
		$builder
			->add('priorities', CollectionType::class, [
				'entry_type' => PriorityType::class,
				'entry_options' => [
					'label' => "false",
					'data' => $eventNames
				],
				'allow_add' => true,
				'allow_delete' => true,
				'prototype' => true,
				'label' => false
			])
			->add('submit', SubmitType::class);
	}
}
