<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserAvailableType extends AbstractType
{
	const female =  'Mme';
	const male = 'M';

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		foreach ($options as $key => $values) {
			if ($key === 'data') {
				$eventDays = $values;
			}
		}
		$builder

			->add('availables', CollectionType::class, [
				'entry_type' => AvailableType::class,
				'entry_options' => [
					'label' => "false",
				'data' => $eventDays
				],
				'allow_add' => true,
				'allow_delete' => true,
				'prototype' => true,
				'label' => false
			])
			->add('submit', SubmitType::class);
	}
}
