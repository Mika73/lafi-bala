<?php

namespace App\Form;

use DateTime;
use App\Entity\Available;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AvailableType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		foreach ($options as $key => $values) {
			if ($key === 'data') {
				$eventDays = $values;
			}
		}
		// $eventDaysArray =$this->setEventDate($eventDays);

		$convenientTime = [
			'Je ne suis pas disponible ce jour' => '',
			Available::MORNING => Available::MORNING,
			Available::AFTERNOON => Available::AFTERNOON,
			Available::EVENING => Available::EVENING,
			Available::MORNING_AFTERNOON => Available::MORNING_AFTERNOON,
			Available::AFTERNOON_EVENING => Available::AFTERNOON_EVENING,
			Available::MORNING_EVENING => Available::MORNING_EVENING,
			Available::ALLDAY => Available::ALLDAY,
		];

		$eventDay1 = DateTime::createFromFormat("d-m-Y", $eventDays[0]['eventdate']);
		$eventDay2 = DateTime::createFromFormat("d-m-Y", $eventDays[1]['eventdate']);
	
		$builder
			// ->add('date', ChoiceType::class, [
			// 	'choices'  => $eventDaysArray
			// ])
			->add('availability1', ChoiceType::class, [
				'choices'  => $convenientTime,
				'label' => 'Le ' . $this->dateToFrench($eventDay1->format('Y-m-d'), 'l j F Y'),
				'required' => false
			])
			->add('availability2', ChoiceType::class, [
				'choices'  => $convenientTime,
				'label' => 'Le ' . $this->dateToFrench($eventDay2->format('Y-m-d'), 'l j F Y'),
				'required' => false
			])

			// ->add('beginTime', TimeType::class, [
			// 	'widget' => 'single_text',
			// 	'attr' => ['class' => ''],
			// 	'label' => 'Heure de début',
			// 	'required'   => true
			// ])
			// ->add('endTime', TimeType::class, [
			// 	'widget' => 'single_text',
			// 	'attr' => ['class' => ''],
			// 	'label' => 'Heure de fin',
			// 	'required'   => true
			// ])
			// ->add('remove', ButtonType::class, [
			// 	'label' => 'Supprimer',
			// 	'attr' => ['class' => 'btn btn-danger btn-sm delButton'],
			// ])
		;
	}

	// function setEventDate($eventDays){
	// 	$eventDaysArray =[];
	// 	foreach($eventDays as $eventDay){
	// 		$eventDaysArray = $this->array_push_assoc($eventDaysArray, $eventDay['eventdate'], $eventDay['eventdate']);
	// 	}
	// 	return $eventDaysArray;
	// }

	// function array_push_assoc($array, $key, $value)
	// {
	// 	$array[$key] = $value;
	// 	return $array;
	// }
	public static function dateToFrench($date, $format)
	{
		$english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
		$french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
		$english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
		return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date))));
	}
}
