<?php

namespace App\Form;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PriorityType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		foreach ($options as $key => $values) {
			if ($key === 'data') {
				$eventNames = $values;
			}
		}
		$eventNamesArray = $this->setEventName($eventNames);

		$builder
			->add('eventname1', ChoiceType::class, [
				'choices'  => $eventNamesArray,
				'label' => '1er choix',
				'required'=> false
			])
			->add('eventname2', ChoiceType::class, [
				'choices'  => $eventNamesArray,
				'label' => '2ᵉ choix',
				'required'=> false
			])
			->add('eventname3', ChoiceType::class, [
				'choices'  => $eventNamesArray,
				'label' => '3ᵉ choix',
				'required'=> false
			]);
	}

	function setEventName($eventNames)
	{
		$eventNamesArray = ['' => ''];
		foreach ($eventNames as $eventName) {
			$eventNamesArray = $this->array_push_assoc($eventNamesArray, $eventName, $eventName);
		}
		return $eventNamesArray;
	}

	function array_push_assoc($array, $key, $value)
	{
		$array[$key] = $value;
		return $array;
	}
}
