<?php

namespace App\Form;

use App\Entity\Task;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class SlotAutoType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('date', DateType::class, [
				'widget' => 'single_text'
			])
			->add('beginTime', TimeType::class, [
				'widget' => 'single_text',
				'label'  => 'De'
			])
			->add('endTime', TimeType::class, [
				'widget' => 'single_text'
			])
			->add('interval', ChoiceType::class, [
				'choices'  =>
				[
					'30 minutes' => 'PT30M',
					'1 heure' => 'PT1H',
				],
			])
			->add('requiredUsers', IntegerType::class)
			->add('task', EntityType::class, [
				'class' => Task::class,
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('t')
						->orderBy('t.name', 'ASC');
				},
				'choice_label' => 'name',
			])
			->add('submit', SubmitType::class)
			->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onPostSubmit']);
	}
	function onPostSubmit(FormEvent $event)
	{
		$form = $event->getForm();

		$begin_time = $form->get('beginTime')->getData();
		$end_time = $form->get('endTime')->getData();
		if ($begin_time > $end_time) {
			$form['beginTime']->addError(new FormError("L'heure de début ne peut pas être postérieure à l'heure de fin"));
		}
	}
}
