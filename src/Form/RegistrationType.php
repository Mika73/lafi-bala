<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RegistrationType extends AbstractType
{
	const FEMALE =  'Mme';
	const MALE = 'M';

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('gender', ChoiceType::class, [
				'choices'  => [
					self::MALE => self::MALE,
					self::FEMALE => self::FEMALE,
				],
				'expanded' => true,
				'multiple' => false
			])
			->add('lastName', TextType::class)
			->add('firstName', TextType::class)
			->add('email', EmailType::class)
			->add('phone', TextType::class)
			->add('password', PasswordType::class)
			->add('birthDate', BirthdayType::class, [
				'format' => 'dd-MM-yyyy',
				'required' => false
			])
			->add('comment', TextareaType::class, [
				'attr' => ['class' => 'specific-textarea'],
				'required' => false
			])
			->add('consent', CheckboxType::class, [
				'required' => true,
				'mapped' => false,
			])
			->add('newsletter', CheckboxType::class, [
				'required' => false,
			]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => User::class,
		]);
	}
}
