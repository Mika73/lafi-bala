<?php

namespace App\Form;

use App\Entity\Slot;
use App\Entity\User;
use App\Entity\Affectation;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AffectationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.lastName', 'ASC');
                },
                'choice_label' => function ($user) {
                    $userId = $user->getId();
                    $userLastName = $user->getLastName();
                    $userFirstName = $user->getFirstName();
                    return ' [ ' . $userId . ' ] ' . $userLastName . ' ' . $userFirstName;
                }
                // 'choice_label' => 'Last name',
            ])
            ->add('slot', EntityType::class, [
                'class' => Slot::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->OrderBy('s.task', 'ASC')
                        ->addOrderBy('s.beginDateTime', 'ASC');
                },
                'choice_label' => function ($slot) {
                    $beginDateTime = $slot->getBeginDateTime();
                    $endDateTime = $slot->getEndDateTime();
                    $taskName = $slot->getTask()->getName();
                    return ' [ ' . $taskName . ' ] ' . $beginDateTime->format('d-m-Y') . ' / ' . $beginDateTime->format('H:i') . ' - ' . $endDateTime->format('H:i');
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Affectation::class,
        ]);
    }
}
