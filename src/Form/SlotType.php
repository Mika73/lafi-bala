<?php

namespace App\Form;

use App\Entity\Slot;
use App\Entity\Task;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class SlotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('beginDateTime', DateTimeType::class,[
                'date_widget'=> 'single_text',
                'label' => 'Date et heure de début'
            ])
            ->add('endDateTime', DateTimeType::class,[
                'date_widget'=> 'single_text',
                'label' => 'Date et heure de fin'
            ])
            ->add('requiredUsers', IntegerType::class,[
                'label' => 'Besoin bénévole'
            ])
            ->add('task', EntityType::class, [
                'class' => Task::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                    ->orderBy('t.name', 'ASC');
                },
                'choice_label' => 'name',
                'label'=> 'Pôle'
            ])
            ->add('meal', ChoiceType::class, [
				'choices'  => [
                    'Pas de repas '=>'Pas de repas',
                    'Repas de midi'=>'Repas de midi',
                    'Repas de soir'=>'Repas de soir'
                ],
				'label' => 'Repas'
			])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Slot::class,
        ]);
    }
}
