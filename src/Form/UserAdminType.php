<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Available;
use App\Repository\SlotRepository;
use App\Repository\TaskRepository;
use App\Repository\PriorityRepository;
use App\Repository\AvailableRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserAdminType extends AbstractType
{
    const FEMALE =  'Mme';
    const MALE = 'M';
    const FIRST_DAY = '2022-06-17';
    const SECOND_DAY = '2022-06-18';
    const FIRST_CHOICE = 1;
    const SECOND_CHOICE = 2;
    const THIRD_CHOICE = 3;

    public function __construct(SlotRepository $slotRepository, TaskRepository $taskRepository, AvailableRepository $availableRepository, PriorityRepository $priorityRepository)
    {
        $this->slotRepository = $slotRepository;
        $this->taskRepository = $taskRepository;
        $this->availableRepository = $availableRepository;
        $this->priorityRepository = $priorityRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['data'];
        $firstday = '';
        $secondday = '';
        $firstchoice = '';
        $secondchoice = '';
        $thirdchoice = '';

        $availables = $this->availableRepository->findByUserId($user->getId());
        $priorities = $this->priorityRepository->findByUserId($user->getId());

        $firstdayArray = [];
        $seconddayArray = [];
        foreach ($availables as $available) {
            if ($available->getBeginDateTime()->format('Y-m-d') == self::FIRST_DAY) {
                $firstdayArray[] = $available->getAvailablePeriod();
            }
            if ($available->getBeginDateTime()->format('Y-m-d') == self::SECOND_DAY) {
                $seconddayArray[] = $available->getAvailablePeriod();
            }
        }
        // Set the currently set available periode as the initial value
        if (count($firstdayArray) == 1) {
            $firstday = $firstdayArray[0];
        }
        if (count($firstdayArray) == 2) {
            $firstday = Available::MORNING_EVENING;
        }

        if (count($seconddayArray) == 1) {
            $secondday = $seconddayArray[0];
        }
        if (count($seconddayArray) == 2) {
            $secondday = Available::MORNING_EVENING;
        }

        foreach ($priorities as $priority) {
            if ($priority->getPriority() == self::FIRST_CHOICE) {
                $firstchoice = $priority->getTask()->getName();
            }
            if ($priority->getPriority() == self::SECOND_CHOICE) {
                $secondchoice = $priority->getTask()->getName();
            }
            if ($priority->getPriority() == self::THIRD_CHOICE) {
                $thirdchoice = $priority->getTask()->getName();
            }
        }
        $eventDays = $this->slotRepository->findEventsDates();

        $convenientTime = [
            'Je ne suis pas disponible ce jour' => '',
            Available::MORNING => Available::MORNING,
            Available::AFTERNOON => Available::AFTERNOON,
            Available::EVENING => Available::EVENING,
            Available::MORNING_AFTERNOON => Available::MORNING_AFTERNOON,
            Available::AFTERNOON_EVENING => Available::AFTERNOON_EVENING,
            Available::MORNING_EVENING => Available::MORNING_EVENING,
            Available::ALLDAY => Available::ALLDAY,
        ];

        $tasks = $this->taskRepository->findAll();
        $eventNamesArray = ['' => ''];
        foreach ($tasks as $task) {
            $eventNamesArray = $this->array_push_assoc($eventNamesArray, $task->getName(), $task->getName());
        }
        $builder
            ->add('email')
            ->add('firstName', TextType::class, [
                'label' => 'Prénom',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Téléphone',
            ])
            ->add('gender', ChoiceType::class, [
                'choices'  => [
                    self::MALE => self::MALE,
                    self::FEMALE => self::FEMALE,
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => 'Civilité'
            ])
            ->add('birthDate', BirthdayType::class, [
                'widget' => 'single_text',
                'required'          => false,
                'label' => 'Date de naissance'
            ])
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices'  => [
                    'Bénévole' => 'ROLE_USER',
                    'Organisateur' => 'ROLE_ADMIN',
                ]
            ])
            ->add('availability1', ChoiceType::class, [
                'choices'  => $convenientTime,
                'label' => 'Votre disponibilité du ' . $eventDays[0]['eventdate'],
                'required' => false,
                'mapped' => false,
                'data' => $firstday

            ])
            ->add('availability2', ChoiceType::class, [
                'choices'  => $convenientTime,
                'label' => 'Votre disponibilité du ' . $eventDays[1]['eventdate'],
                'required' => false,
                'mapped' => false,
                'data' => $secondday
            ])
            ->add('eventname1', ChoiceType::class, [
                'choices'  => $eventNamesArray,
                'label' => '1er choix',
                'required' => false,
                'mapped' => false,
                'data' => $firstchoice
            ])
            ->add('eventname2', ChoiceType::class, [
                'choices'  => $eventNamesArray,
                'label' => '2ᵉ choix',
                'required' => false,
                'mapped' => false,
                'data' => $secondchoice
            ])
            ->add('eventname3', ChoiceType::class, [
                'choices'  => $eventNamesArray,
                'label' => '3ᵉ choix',
                'required' => false,
                'mapped' => false,
                'data' => $thirdchoice
            ])
            ->add('newsletter', CheckboxType::class, [
                'required' => false,
                'label' => 'Je souhaite recevoir les newsletters de Chambéry Solidarité Internationale'
            ])
            ->add('comment', TextareaType::class, [
                'attr' => ['class' => 'specific-textarea'],
                'required'          => false,
                'label' => 'Commentaires aux organisateurs'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
    function array_push_assoc($array, $key, $value)
    {
        $array[$key] = $value;
        return $array;
    }
}
