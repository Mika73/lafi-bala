<?php

namespace App\Repository;

use App\Entity\Affectation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Affectation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Affectation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Affectation[]    findAll()
 * @method Affectation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffectationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Affectation::class);
    }

    /**
     * @return Affectation[] Returns an array of Affectation objects
     */

    public function findAllOrderBy()
    {
        return $this->createQueryBuilder('a')
            ->leftJoin("App:Slot", "s", "WITH", "a.slot = s.id")
            ->leftJoin("App:Task", "t", "WITH", "s.task = t.id")
            ->orderBy('t.id', 'ASC')
            ->addorderBy('s.beginDateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }
    public function deleteAll()
    {
        $query = $this->createQueryBuilder('a')
                 ->delete()
                 ->getQuery()
                 ->execute();
        return $query;
    }

    /*
    public function findOneBySomeField($value): ?Affectation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
