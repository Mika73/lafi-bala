<?php

namespace App\Repository;

use App\Entity\Slot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Slot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Slot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Slot[]    findAll()
 * @method Slot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SlotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Slot::class);
    }

    /**
     * @return Slot[] Returns an array of Slot objects
     */

    public function findAllOrderByDateTime()
    {
        return $this->createQueryBuilder('s')
            ->OrderBy('s.beginDateTime', 'ASC')
            ->addorderBy('s.task', 'ASC')
            ->getQuery()
            ->getResult();
    }
    public function findAllOrderByTaskName()
    {
        return $this->createQueryBuilder('s')
            ->OrderBy('s.task', 'ASC')
            ->addorderBy('s.beginDateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Slot[] Returns an array of Slot objects
     */

    public function findEventsDates()
    {
        return $this->createQueryBuilder("s")
            ->select('DATE_FORMAT(s.beginDateTime, \'%d-%m-%Y\') as eventdate')
            ->orderBy('s.beginDateTime', 'ASC')
            ->groupBy('eventdate')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Slot[] Returns an array of Slot objects
     */
    public function findAvailableEvents($beginDateTime, $endDateTime)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.beginDateTime >= :beginDateTime')
            ->andWhere('s.endDateTime <= :endDateTime')
            ->setParameter('beginDateTime', $beginDateTime)
            ->setParameter('endDateTime', $endDateTime)
            ->groupBy('s.task')
            ->getQuery()
            ->getResult();
    }
    /*
    public function findOneBySomeField($value): ?Slot
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findSlotsByAvailables()
    {
        return $this->createQueryBuilder('s')
            ->select("s")
            ->addSelect("count(av.id) as availables")
            ->leftJoin("App:Available", "av", "WITH", "av.beginDateTime <= s.beginDateTime AND av.endDateTime >= s.endDateTime")
            ->groupBy('s.id')
            ->orderBy('availables')
            ->getQuery()
            ->getResult();
    }

    public function findSlotsByAvailablesOrderByDateTime()
    {
        return $this->createQueryBuilder('s')
            ->select("s")
            ->addSelect("count(av.id) as availables")
            ->leftJoin("App:Available", "av", "WITH", "av.beginDateTime <= s.beginDateTime AND av.endDateTime >= s.endDateTime")
            ->groupBy('s.id')
            ->OrderBy('s.beginDateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }
    public function findSlotsByAvailablesOrderByTaskName()
    {
        return $this->createQueryBuilder('s')
            ->select("s")
            ->addSelect("count(av.id) as availables")
            ->leftJoin("App:Available", "av", "WITH", "av.beginDateTime <= s.beginDateTime AND av.endDateTime >= s.endDateTime")
            ->groupBy('s.id')
            ->OrderBy('s.task', 'ASC')
            ->addOrderBy('s.beginDateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

}
