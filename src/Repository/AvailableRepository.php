<?php

namespace App\Repository;

use App\Entity\Available;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Available|null find($id, $lockMode = null, $lockVersion = null)
 * @method Available|null findOneBy(array $criteria, array $orderBy = null)
 * @method Available[]    findAll()
 * @method Available[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvailableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Available::class);
    }

    /**
     * @return Available[] Returns an array of Available objects
     */

    public function findByUserId($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.user = :val')
            ->setParameter('val', $value)
            ->orderBy('a.beginDateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findAllUserIdNotNull()
    {
        return $this->createQueryBuilder('a')
            ->andwhere('a.user IS NOT NULL')
            ->orderBy('a.beginDateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findOneByTimeAndUser($user, $beginDateTime, $endDateTime): ?Available
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.user = :userid')
            ->andWhere('a.beginDateTime = :begin')
            ->andWhere('a.endDateTime = :end')
            ->setParameter('userid', $user->getId())
            ->setParameter('begin', $beginDateTime)
            ->setParameter('end', $endDateTime)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
