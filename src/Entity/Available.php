<?php

namespace App\Entity;

use App\Repository\AvailableRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AvailableRepository::class)
 */
class Available
{
    const MORNING=  'Matin';
	const AFTERNOON = 'Après-midi';
	const EVENING = 'Soir';
	const MORNING_AFTERNOON =  'Matin + Après-midi';
	const AFTERNOON_EVENING =  'Après-midi + Soir';
    const MORNING_EVENING = 'Matin + Soir';
	const ALLDAY = 'Toute la journée';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $beginDateTime;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDateTime;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="availables")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $availablePeriod;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeginDateTime(): ?\DateTimeInterface
    {
        return $this->beginDateTime;
    }

    public function setBeginDateTime(\DateTimeInterface $beginDateTime): self
    {
        $this->beginDateTime = $beginDateTime;

        return $this;
    }

    public function getEndDateTime(): ?\DateTimeInterface
    {
        return $this->endDateTime;
    }

    public function setEndDateTime(\DateTimeInterface $endDateTime): self
    {
        $this->endDateTime = $endDateTime;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAvailablePeriod(): ?string
    {
        return $this->availablePeriod;
    }

    public function setAvailablePeriod(string $availablePeriod): self
    {
        $this->availablePeriod = $availablePeriod;

        return $this;
    }
}
